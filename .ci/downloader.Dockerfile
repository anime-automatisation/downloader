FROM openjdk:23-slim

EXPOSE 8080

WORKDIR /usr/application/

VOLUME ["/usr/application/downloads"]
VOLUME ["/usr/application/animes"]
VOLUME ["/usr/application/config"]

ADD build/install/downloader/bin /usr/application/bin
ADD build/install/downloader/lib /usr/application/lib

CMD ["/usr/application/bin/downloader"]