import de.comhix.gradle.plugins.version.Version
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

project.group = "de.comhix.anime"
project.version = Version(3, 0, System.getenv("CI_PIPELINE_IID")?.toIntOrNull())

repositories {
    mavenCentral()
    maven {
        url = uri("https://maven.pkg.jetbrains.space/public/p/ktor/eap")
    }
}

dependencies {
    implementation(kotlin("stdlib"))
//    implementation(kotlin("reflect"))
    implementation(project(":sharedCode"))
    implementation(project(":anidbApi"))
    implementation(libs.kotlinx.serialization.core)
    implementation(libs.kotlinx.serialization.json)
    implementation(libs.kotlinx.datetime)
    implementation(libs.kotlinx.coroutines.core)
    implementation(libs.koin.core)

    implementation(libs.exposed.core)
    implementation(libs.exposed.dao)
    implementation(libs.exposed.jdbc)
    implementation(libs.exposed.crypt)
    implementation(libs.exposed.kotlin.datetime)
    implementation(libs.postgresql)

    implementation(libs.ktor.client.core)
    implementation(libs.ktor.client.okhttp)
    implementation(libs.ktor.client.content.negotiation)
    implementation(libs.ktor.serialization.kotlinx.json)

    implementation(libs.krypto)
    implementation(libs.korlibs.luak)


//    implementation("de.comhix.commons:updater:${buildVersions.DependencyVersions.commonsVersion}")
//    implementation("de.comhix.commons:logging:${buildVersions.DependencyVersions.commonsVersion}")
//    implementation("de.comhix.commons:kotlin:${buildVersions.DependencyVersions.commonsVersion}")
//    implementation("com.google.guava:guava:33.2.0-jre")
//    implementation("com.google.code.gson:gson:2.10.1")
//    implementation("io.insert-koin:koin-core:${buildVersions.DependencyVersions.koinVersion}")
//    implementation("io.ktor:ktor-client-core:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-client-cio:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-client-okhttp:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-client-plugins:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-client-serialization:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-client-content-negotiation:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-serialization-kotlinx-json:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-server-html-builder:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-server-core:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-server-netty:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-serialization:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-server-status-pages:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-server-call-logging:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("io.ktor:ktor-server-content-negotiation:${buildVersions.DependencyVersions.ktorVersion}")
//    implementation("commons-io:commons-io:2.16.1")

    testImplementation(kotlin("test"))
    testImplementation(kotlin("test-testng"))
    testImplementation(libs.kluent)
    testImplementation(libs.mockk)
    testImplementation(libs.koin.test)
    testImplementation(libs.kotlinx.coroutines.test)
}

plugins {
    application
    alias(libs.plugins.kotlin.multiplatform) apply false
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.serialization)
    id("jacoco")
    id("maven-publish")
    alias(libs.plugins.dokka)
    alias(libs.plugins.ossScan)
    alias(libs.plugins.version)
}

jacoco.toolVersion = "0.8.12"

sourceSets {
    main {
        java {
            srcDir("${rootDir}/src/generated/kotlin")
        }
    }
}

kotlin {
    compilerOptions {
        jvmTarget.set(JvmTarget.JVM_21)
        freeCompilerArgs.add("-Xopt-in=kotlin.RequiresOptIn")
        freeCompilerArgs.add("-Xjsr305=strict")
    }
    jvmToolchain(21)
}

tasks.test {
    useTestNG()
}

application {
    mainClass.set("${project.group}.${project.name}.MainKt")
}
tasks.jar {
    manifest {
        attributes["Main-Class"] = "${project.group}.${project.name}.MainKt"
        attributes["Class-Path"] = configurations.runtimeClasspath.get().joinToString(" ") { it.name }
        attributes["Implementation-Version"] = project.version
    }
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
        csv.required.set(false)
        html.outputLocation.set(file("${layout.buildDirectory.get()}/reports/jacocoHtml"))
    }
}

tasks.check {
    dependsOn(tasks.jacocoTestReport)
}

val dokkaJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Kotlin docs with Dokka"
    archiveClassifier.set("javadoc")
    from(tasks.dokkaHtml)
    destinationDirectory.set(file("${layout.buildDirectory.get()}/output"))
}

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
    destinationDirectory.set(file("${layout.buildDirectory.get()}/output"))
}

publishing {
    publications {
        create<MavenPublication>("default") {
            from(components["java"])
            artifact(dokkaJar)
            artifact(sourcesJar)
        }
    }
    repositories {
        maven {
            url = uri("${layout.buildDirectory.get()}/repository")
        }
    }
}

ossIndexAudit {
    isShowAll = false
    isAllConfigurations = false
    isPrintBanner = false
    outputFormat = org.sonatype.gradle.plugins.scan.ossindex.OutputFormat.DEPENDENCY_GRAPH
}

//signing {
//    isRequired = !(project.version as Version).isSnapshot
//}

val runJar by tasks.registering(Exec::class) {
    dependsOn(tasks.jar)
    group = "execution"
    commandLine("java", "-jar", tasks.jar.get().archiveFile.get().asFile.canonicalPath)
}

val buildTimestamp: Task by tasks.creating {
    val outputDir: File = file("${layout.buildDirectory.get()}")
    if (!outputDir.exists()) outputDir.mkdirs()

    Files.write(Paths.get(outputDir.absolutePath, "buildTimestamp"), "${Date().time}".toByteArray())
}


tasks.register<JacocoReport>("codeCoverageReport") {
    group = "reporting"

    executionData(fileTree(project.rootDir.absolutePath).include("**/build/jacoco/*.exec"))
    //    sourceSets(project.extensions.getByType(SourceSetContainer::class.java).getByName("main"))
    sourceSets(
        *allprojects.filterNot { it == it.rootProject }
            .mapNotNull {
                println("sourcesset of ${it.name}")
                it.extensions.findByType(SourceSetContainer::class.java)?.findByName("main")
            }
            .toTypedArray()
    )

    reports {
        csv.required.set(false)
        xml.required.set(true)
        html.required.set(true)
        xml.outputLocation.set(file("${layout.buildDirectory.get()}/jacoco/jacoco.xml"))
        html.outputLocation.set(file("${layout.buildDirectory.get()}/reports/jacocoHtml"))
    }

    val dependsOn: Set<String> = setOf("test", "jacocoTestReport")
    val runAfter: Set<String> = setOf(
        "copyToLib",
        "productionExecutableCompileSync",
        "browserProductionWebpack",
        "compileCommonMainKotlinMetadata",
        "allTests",
        "assemble"
    )

    dependsOn(allprojects
                  .flatMap { subProject ->
                      dependsOn.mapNotNull { subProject.tasks.findByName(it) }
                  })
    mustRunAfter(allprojects
                     .flatMap { subProject ->
                         runAfter.mapNotNull { subProject.tasks.findByName(it) }
                     })
}