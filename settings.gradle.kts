rootProject.name = "downloader"

include("sharedCode", "anidbApi")


dependencyResolutionManagement {
    versionCatalogs {
        create("libs") {
            version("ktor", "3.2.0-eap-1254")
            version("kotlinx-serialization", "1.8.0")
            version("kotlinx-coroutines", "1.10.1")
            version("koin", "4.0.2")
            version("exposed", "0.60.0")
            version("korlibs", "3.4.0")
            version("kotlin", "2.1.10")

            plugin("kotlin-multiplatform","org.jetbrains.kotlin.multiplatform").versionRef("kotlin")
            plugin("kotlin-jvm","org.jetbrains.kotlin.jvm").versionRef("kotlin")
            plugin("kotlin-serialization","org.jetbrains.kotlin.plugin.serialization").versionRef("kotlin")
            plugin("dokka", "org.jetbrains.dokka").version("2.0.0")
            plugin("ossScan", "org.sonatype.gradle.plugins.scan").version("3.0.0")
            plugin("version", "de.comhix.gradle.plugins.version").version("1.5.43")

            library("postgresql", "org.postgresql", "postgresql").version("42.7.5")
            library("h2", "com.h2database", "h2").version("2.3.232")

            library("mockk", "io.mockk", "mockk").version("1.13.17")
            library(
                "micrometer-registry-prometheus",
                "io.micrometer",
                "micrometer-registry-prometheus"
            ).version("1.14.5")
            library("logback-classic", "ch.qos.logback", "logback-classic").version("1.5.17")
            library("ktor-server-metrics-micrometer", "io.ktor", "ktor-server-metrics-micrometer").versionRef("ktor")
            library("ktor-server-cors", "io.ktor", "ktor-server-cors").versionRef("ktor")
            library("ktor-server-core", "io.ktor", "ktor-server-core").versionRef("ktor")
            library("ktor-server-content-negotiation", "io.ktor", "ktor-server-content-negotiation").versionRef("ktor")
            library("ktor-server-cio", "io.ktor", "ktor-server-cio").versionRef("ktor")
            library("ktor-server-call-logging", "io.ktor", "ktor-server-call-logging").versionRef("ktor")
            library("ktor-server-auth", "io.ktor", "ktor-server-auth").versionRef("ktor")
            library("ktor-serialization-kotlinx-json", "io.ktor", "ktor-serialization-kotlinx-json").versionRef("ktor")
            library("ktor-serialization-kotlinx-xml", "io.ktor", "ktor-serialization-kotlinx-xml").versionRef("ktor")
            library("ktor-client-okhttp", "io.ktor", "ktor-client-okhttp").versionRef("ktor")
            library("ktor-client-logging", "io.ktor", "ktor-client-logging").versionRef("ktor")
            library("ktor-client-js", "io.ktor", "ktor-client-js").versionRef("ktor")
            library("ktor-client-core", "io.ktor", "ktor-client-core").versionRef("ktor")
            library("ktor-client-content-negotiation", "io.ktor", "ktor-client-content-negotiation").versionRef("ktor")
            library("krypto", "com.soywiz.korlibs.krypto", "krypto").version("4.0.10")
            library("korlibs-luak", "com.soywiz.korlibs.luak", "luak").versionRef("korlibs")

            library(
                "kotlinx-serialization-json",
                "org.jetbrains.kotlinx",
                "kotlinx-serialization-json"
            ).versionRef("kotlinx-serialization")
            library(
                "kotlinx-serialization-core",
                "org.jetbrains.kotlinx",
                "kotlinx-serialization-core"
            ).versionRef("kotlinx-serialization")
            library("kotlinx-html", "org.jetbrains.kotlinx", "kotlinx-html").version("0.12.0")
            library("kotlinx-datetime", "org.jetbrains.kotlinx", "kotlinx-datetime").version("0.6.2")
            library(
                "kotlinx-coroutines-test",
                "org.jetbrains.kotlinx",
                "kotlinx-coroutines-test"
            ).versionRef("kotlinx-coroutines")
            library(
                "kotlinx-coroutines-core",
                "org.jetbrains.kotlinx",
                "kotlinx-coroutines-core"
            ).versionRef("kotlinx-coroutines")
            library("koin-test", "io.insert-koin", "koin-test").versionRef("koin")
            library("koin-core", "io.insert-koin", "koin-core").versionRef("koin")
            library("kluent", "org.amshove.kluent", "kluent").version("1.73")
            library("exposed-kotlin-datetime", "org.jetbrains.exposed", "exposed-kotlin-datetime").versionRef("exposed")
            library("exposed-jdbc", "org.jetbrains.exposed", "exposed-jdbc").versionRef("exposed")
            library("exposed-dao", "org.jetbrains.exposed", "exposed-dao").versionRef("exposed")
            library("exposed-crypt", "org.jetbrains.exposed", "exposed-crypt").versionRef("exposed")
            library("exposed-core", "org.jetbrains.exposed", "exposed-core").versionRef("exposed")

            library(
                "kotlin-wrappers-bom",
                "org.jetbrains.kotlin-wrappers",
                "kotlin-wrappers-bom"
            ).version("2025.3.9")
            library("kotlin-wrappers-react-core", "org.jetbrains.kotlin-wrappers", "kotlin-react").withoutVersion()
            library("kotlin-wrappers-react-dom", "org.jetbrains.kotlin-wrappers", "kotlin-react-dom").withoutVersion()
            library(
                "kotlin-wrappers-react-router",
                "org.jetbrains.kotlin-wrappers",
                "kotlin-react-router-dom"
            ).withoutVersion()
            library("kotlin-wrappers-emotion", "org.jetbrains.kotlin-wrappers", "kotlin-emotion").withoutVersion()
        }
    }
}