package de.comhix.utils

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job

/**
 * @author Benjamin Beeker
 */
val Job.name: String?
    get() = (this as CoroutineScope).coroutineContext[CoroutineName.Key]?.name