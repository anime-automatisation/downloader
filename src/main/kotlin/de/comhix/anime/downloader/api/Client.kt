package de.comhix.anime.downloader.api

import de.comhix.anime.downloader.config.Config
import de.comhix.anime.sharedCode.api.File
import de.comhix.anime.sharedCode.logger.info
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.sse.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.utils.io.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flow
import kotlinx.serialization.json.Json
import de.comhix.anime.sharedCode.api.FileApi as SharedFileApi

/**
 * @author Benjamin Beeker
 */
class Client(private val config: Config) {
    private val httpClient = HttpClient {
        install(ContentNegotiation) {
            json()
        }
        install(SSE)
        defaultRequest {
            url(config.apiEndpoint)
            header("Authorization", "Bearer ${config.apiToken}")
        }
        expectSuccess = true
    }

    internal val fileApi = object : FileApi {
        override suspend fun getList(): List<File> =
            httpClient.get(URLBuilder(config.apiEndpoint).appendPathSegments(baseUrl).build()).body()

        override suspend fun observeList(): Flow<File> = flow {
            httpClient.sse(
                URLBuilder(config.apiEndpoint)
                    .appendPathSegments(baseUrl)
                    .appendPathSegments("observe-list")
                    .buildString()
            ) {
                incoming
                    .filter { it.event == "File" }
                    .collect {
                        emit(Json.decodeFromString(it.data!!))
                    }
            }
        }

        override suspend fun download(filename: String, output: ByteWriteChannel) {
            val prepared = httpClient.prepareGet {
                val targetUrl = URLBuilder(config.downloadEndpoint).appendPathSegments(filename).build()
                info{"downloading $targetUrl"}
                url(targetUrl)
            }

            prepared.execute {
                it.bodyAsChannel().copyAndClose(output)
            }
        }
    }
}

internal interface FileApi : SharedFileApi {
    suspend fun download(filename: String, output: ByteWriteChannel)
}