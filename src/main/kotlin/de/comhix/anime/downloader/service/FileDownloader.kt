package de.comhix.anime.downloader.service

import de.comhix.anime.downloader.api.Client
import de.comhix.anime.downloader.config.Config
import de.comhix.anime.downloader.data.Download
import de.comhix.anime.downloader.data.DownloadStatus.*
import de.comhix.anime.downloader.data.DownloadTable
import de.comhix.anime.sharedCode.logger.debug
import de.comhix.anime.sharedCode.logger.info
import de.comhix.anime.sharedCode.logger.warn
import io.ktor.client.plugins.*
import io.ktor.util.cio.*
import korlibs.crypto.SHA256
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.withContext
import kotlinx.datetime.Instant
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import java.io.File

/**
 * @author Benjamin Beeker
 */
class FileDownloader(private val client: Client, config: Config) {

    private val downloadFolder = File(config.downloadFolder).also { it.mkdirs() }

    suspend fun syncFileList() {
        newSuspendedTransaction {
            client.fileApi.getList()
                .filter {
                    Download.find {
                        DownloadTable.name eq it.name
                    }.empty()
                }
                .forEach {
                    Download.new {
                        name = it.name
                        lastModified = it.lastModified
                        size = it.size
                        sha256 = it.sha256
                        status = New
                    }
                }
        }
    }

    suspend fun registerFileSync() {
        client.fileApi
            .observeList()
            .filter {
                newSuspendedTransaction {
                    Download.find {
                        DownloadTable.name eq it.name
                    }.empty()
                }
            }
            .collect {
                newSuspendedTransaction {
                    Download.new {
                        name = it.name
                        lastModified = it.lastModified
                        size = it.size
                        sha256 = it.sha256
                        status = New
                    }
                }
            }
    }

    suspend fun existingFiles() {
        downloadFolder.recursiveFileList()
            .forEach {
                newSuspendedTransaction {
                    val relativeName = it.toRelativeString(downloadFolder)
                    val download =
                        Download.find { DownloadTable.name eq relativeName }.firstOrNull()
                    if (download != null) {
                        if (download.status == Renamed) {
                            download.status = Hashed
                            download.lastModified = Instant.fromEpochMilliseconds(it.lastModified())
                        }
                    } else {
                        val checksum = it.calculateChecksum()
                        Download.new {
                            name = relativeName
                            lastModified = Instant.fromEpochMilliseconds(it.lastModified())
                            size = it.length()
                            sha256 = checksum
                            status = Downloaded
                        }
                    }
                }
            }
    }

    suspend fun downloadFile(download: Download) {
        newSuspendedTransaction {
            download.status = Downloading
        }

        val outputFile = downloadFolder.resolve(download.name)
        val newStatus = try {
            outputFile.parentFile.mkdirs()
            info { "starting file download ${outputFile.absolutePath}" }
            outputFile.writeChannel()
                .use {
                    client.fileApi.download(download.name, this)
                }

            val checksum = outputFile.calculateChecksum()
            if (checksum == download.sha256) {
                Downloaded
            } else {
                outputFile.delete()
                Error
            }
        } catch (ex: Exception) {
            outputFile.delete()
            if (ex is ClientRequestException && ex.response.status.value == 404) {
                Removed
            } else {
                warn { "exception: ${ex.message}" }
                debug { ex.stackTraceToString() }
                Error
            }
        }

        newSuspendedTransaction {
            if (newStatus == Error)
                download.delete()
            else
                download.status = newStatus
        }
    }

    fun fileExists(download: Download): Boolean = downloadFolder.resolve(download.name).exists()

    private suspend fun File.calculateChecksum(): String = withContext(Dispatchers.IO) {
        val sha256 = SHA256()
        forEachBlock { buffer, bytesRead -> sha256.update(buffer, 0, bytesRead) }

        sha256.digest().hex
    }

    private fun File.recursiveFileList(): List<File> {
        return if (this.isDirectory) {
            val files = this.listFiles()!!
            if (files.isEmpty() && this != downloadFolder) {
                this.delete()
            }
            files.flatMap { it.recursiveFileList() }
        } else {
            listOf(this)
        }
    }
}