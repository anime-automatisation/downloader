package de.comhix.anime.downloader.job

/**
 * @author Benjamin Beeker
 */
sealed interface AnimeJob {
    suspend fun execute()
}