package de.comhix.anime.downloader.job

import de.comhix.anidb.Anidb
import de.comhix.anidb.request.mask.FileAnimeMask
import de.comhix.anime.downloader.data.Download
import de.comhix.anime.downloader.data.DownloadStatus.*
import de.comhix.anime.downloader.data.anidb.*
import de.comhix.anime.downloader.data.anidb.FileTable
import kotlinx.datetime.Clock
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import kotlin.time.Duration.Companion.days
import de.comhix.anidb.data.Anime as AnidbAnime
import de.comhix.anidb.data.Episode as AnidbEpisode
import de.comhix.anidb.data.File as AnidbFile

/**
 * @author Benjamin Beeker
 */
class AniDBJob(
    private val anidb: Anidb,
    private val download: Download
) : AnimeJob {
    override suspend fun execute() {
        if (!anidb.isLoggedIn()) {
            anidb.login()
        }

        val fileId = newSuspendedTransaction {
            download.lastLookup = Clock.System.now()
            File.find {
                FileTable.ed2k eq this@AniDBJob.download.ed2k!!
            }.firstOrNull()?.id?.value ?: run {
                anidb.file(this@AniDBJob.download.size, this@AniDBJob.download.ed2k!!).call().file?.id
            }
        }

        val file = File.updateOrCreate(fileId) {
            anidb.file(it).animeMask(FileAnimeMask()).call().file?.toDb
        }

        val episode = Episode.updateOrCreate(file?.episodeId) {
            anidb.episode(it)?.toDb
        }

        val anime = Anime.updateOrCreate(episode?.animeId) {
            anidb.anime(it).call().anime.toDb
        }

        newSuspendedTransaction {
            download.status = if (anime != null) Matched else Hashed
        }
    }
}

private val RefreshInterval = 30.days

private suspend fun <T> IntEntityClass<T>.updateOrCreate(
    id: Int?,
    loader: suspend (Int) -> (T.() -> Unit)?
): T? where T : Timestamped, T : IntEntity {
    id ?: return null

    return newSuspendedTransaction {
        val existing = this@updateOrCreate.findById(id)

        if (existing != null) {
            if (existing.lastUpdate.plus(RefreshInterval) < Clock.System.now()) {
                loader(id)?.let { it(existing) }
            }

            existing
        } else {
            loader(id)?.let { this@updateOrCreate.new(id, it) }
        }
    }
}

private val AnidbFile.toDb: File.() -> Unit
    get() = {
        this.episodeId = this@toDb.episodeId!!
        this.ed2k = this@toDb.ed2k!!
        this.crc = this@toDb.crc!!
        this.fileSource = this@toDb.source!!
        this.fileType = this@toDb.fileType!!
        this.state = this@toDb.state!!
        this.lastUpdate = Clock.System.now()
    }

private val AnidbEpisode.toDb: Episode.() -> Unit
    get() = {
        this.animeId = this@toDb.animeId!!
        this.length = this@toDb.length!!
        this.rating = this@toDb.rating!!
        this.votes = this@toDb.votes!!
        this.typedEpisodeNumber = this@toDb.typedEpisodeNumber!!
        this.romajiName = this@toDb.romajiName!!
        this.kanjiName = this@toDb.kanjiName!!
        this.englishName = this@toDb.englishName!!
        this.airedDate = this@toDb.airedDate!!
        this.episodeType = this@toDb.episodeType!!
        this.lastUpdate = Clock.System.now()
    }

private val AnidbAnime.toDb: Anime.() -> Unit
    get() = {
        this.dateFlags = this@toDb.dateFlags!!
        this.year = this@toDb.year!!
        this.type = this@toDb.type!!
        this.romajiName = this@toDb.romajiName!!
        this.kanjiName = this@toDb.kanjiName!!
        this.englishName = this@toDb.englishName!!
        this.otherName = this@toDb.otherName ?: ""
        this.episodes = this@toDb.episodes!!
        this.highestEpisodeNumber = this@toDb.highestEpisodeNumber!!
        this.specialEpisodes = this@toDb.specialEpisodes!!
        this.airDate = this@toDb.airDate!!
        this.endDate = this@toDb.endDate!!
        this.url = this@toDb.url!!
        this.picname = this@toDb.picname!!
        this.rating = this@toDb.rating!!
        this.votes = this@toDb.votes!!
        this.temporaryRating = this@toDb.temporaryRating!!
        this.temporaryVotes = this@toDb.temporaryVotes!!
        this.averageReviewRating = this@toDb.averageReviewRating!!
        this.reviews = this@toDb.reviews!!
        this.adult = this@toDb.adult!!
        this.annId = this@toDb.annId!!
        this.allcinemaId = this@toDb.allcinemaId!!
        this.animeNfoId = this@toDb.animeNfoId!!
        this.specials = this@toDb.specials ?: 0
        this.credits = this@toDb.credits ?: 0
        this.others = this@toDb.others ?: 0
        this.trailers = this@toDb.trailers ?: 0
        this.parodys = this@toDb.parodys ?: 0
        this.lastUpdate = Clock.System.now()
    }