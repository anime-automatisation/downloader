package de.comhix.anime.downloader.job

import de.comhix.anidb.Anidb
import de.comhix.anime.downloader.config.Config
import de.comhix.anime.downloader.data.Download
import de.comhix.anime.downloader.data.DownloadStatus.*
import de.comhix.anime.downloader.data.DownloadTable
import de.comhix.anime.downloader.service.FileDownloader
import de.comhix.anime.sharedCode.logger.debug
import de.comhix.anime.sharedCode.logger.info
import de.comhix.anime.sharedCode.logger.warn
import de.comhix.utils.name
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.future.asCompletableFuture
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withPermit
import kotlinx.datetime.Clock
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.koin.core.Koin
import org.koin.core.parameter.parametersOf
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference
import kotlin.reflect.KClass
import kotlin.system.exitProcess
import kotlin.time.Duration
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

/**
 * @author Benjamin Beeker
 */
class Scheduler(
    private val config: Config,
    private val fileDownloader: FileDownloader,
    private val anidb: Anidb,
    private val koin: Koin
) {
    private lateinit var fileSyncJob: Job
    private lateinit var existingFileJob: Job
    private lateinit var fileDownloadJob: Job
    private lateinit var ed2kHashJob: Job
    private lateinit var anidbJob: Job
    private lateinit var anidbLogoutJob: Job
    private lateinit var anidbFlushJob:Job
    private lateinit var renameJob: Job
    private var flushAnidbJob = AtomicReference(Job().also { it.start() })
    private val anidbJobSemaphore = Semaphore(config.scheduler.anidbBuffer)

    private val animeJobWorkers: MutableMap<KClass<out AnimeJob>, List<Job>> = mutableMapOf()

    private val jobs: List<Job>
        get() = listOf(
            fileSyncJob,
            existingFileJob,
            fileDownloadJob,
            ed2kHashJob,
            anidbJob,
            anidbLogoutJob,
            anidbFlushJob,
            renameJob,
            *(animeJobWorkers.values.flatten().toTypedArray())
        )

    private val animeJobChannels: MutableMap<KClass<out AnimeJob>, Channel<AnimeJob>> = mutableMapOf()
    private fun channelForJob(kClass: KClass<out AnimeJob>) = synchronized(this) {
        animeJobChannels
            .getOrPut(kClass) {
                info { "creating channel and runner for $kClass" }

                val channel: Channel<AnimeJob> = if (kClass == AniDBJob::class)
                    Channel(config.scheduler.anidbBuffer)
                else
                    Channel()

                animeJobWorkers.computeIfAbsent(kClass) {
                    val workerCount = when (kClass) {
                        DownloadFile::class -> config.scheduler.maxParallelDownloads
                        Ed2kHash::class -> config.scheduler.maxParallelHashing
                        RenameFile::class -> config.scheduler.maxParallelRenaming
                        else -> 1
                    }

                    (1..workerCount).map { workerIndex ->
                        debug { "starting worker $workerIndex" }

                        CoroutineScope(Dispatchers.IO)
                            .launch(CoroutineName("${kClass.simpleName} worker[$workerIndex]")) {
                                while (true) {
                                    if (kClass == AniDBJob::class) {
                                        anidbLock(kClass, channel, workerIndex)
                                    }
                                    debug { "waiting for job (${kClass.simpleName}[$workerIndex])" }
                                    val job = channel.receive()
                                    debug { "starting job (${kClass.simpleName}[$workerIndex])" }
                                    job.execute()
                                    debug { "finished job (${kClass.simpleName}[$workerIndex])" }
                                    checkFlush()
                                }
                            }
                    }
                }

                channel
            }
    }

    private fun flushAnidbJobs() {
        debug { "flush anidbJob" }
        flushAnidbJob.get().complete()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun checkFlush() {
        try {
            debug { "check flush" }
            if (animeJobChannels.entries.filter { it.key != AniDBJob::class }.all { it.value.isEmpty }) {
                flushAnidbJobs()
            }
        } catch (e: Exception) {
            debug { e.stackTraceToString() }
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private suspend fun anidbLock(kClass: KClass<out AnimeJob>, channel: Channel<AnimeJob>, workerIndex: Int) {
        if (channel.isEmpty && !flushAnidbJob.get().isActive) {
            flushAnidbJob.set( Job().also { it.start() })
        }
        if (flushAnidbJob.get().isActive) {
            debug { "waiting for flush (${kClass.simpleName}[$workerIndex])" }
            flushAnidbJob.get().join()
        }
    }

    fun startSchedules() {
        fileSyncJob = scheduleFileSync()
        existingFileJob = scheduleExistingFiles()
        fileDownloadJob = scheduleFileDownload()
        ed2kHashJob = scheduleEd2kHash()
        anidbJob = scheduleAnidb()
        anidbLogoutJob = scheduleAnidbLogout()
        anidbFlushJob = scheduleAnidbFlush()
        renameJob = scheduleRename()

        if (config.scheduler.suicideMonitor) {
            CoroutineScope(Dispatchers.Default).launch {
                while (true) {
                    delay(5.seconds)
                    if (!healthy()) {
                        warn { "Scheduler not healthy, committing suicide" }
                        jobs.forEach {
                            debug { "${it.name}: ${it.isActive}" }
                        }

                        anidb.logout()

                        exitProcess(1)
                    }
                }
            }
        }
    }

    private fun healthy(): Boolean {
        return jobs.all { it.isActive }
    }

    private fun scheduleFileSync() = launchDaemon("file sync") {
        fileDownloader.registerFileSync()
    }

    private fun scheduleExistingFiles() = launchDaemon("existing files") {
        fileDownloader.existingFiles()
    }

    private fun scheduleFileDownload() = launchDaemon("file download") {
        newSuspendedTransaction {
            Download.find {
                (DownloadTable.status eq New) or (DownloadTable.status eq Error)
            }
                .forEach {
                    info { "scheduling ${it.name} for download" }
                    newSuspendedTransaction {
                        it.status = Downloading
                    }
                    koin.get<DownloadFile>(parameters = { parametersOf(it) }).schedule()
                }
        }
    }

    private fun scheduleEd2kHash() = launchDaemon("ed2k") {
        newSuspendedTransaction {
            Download.find {
                DownloadTable.status eq Downloaded
            }.forEach {
                newSuspendedTransaction {
                    it.status = Hashing
                }
                koin.get<Ed2kHash>(parameters = { parametersOf(it) }).schedule()
            }
        }
    }

    private fun scheduleAnidb() = launchDaemon("anidb") {
        newSuspendedTransaction {
            Download.find {
                (DownloadTable.status eq Hashed) and
                        (DownloadTable.ed2k neq null) and
                        (DownloadTable.lastLookup less Clock.System.now().minus(1.hours))
            }
                .filter {
                    fileDownloader.fileExists(it)
                }
                .forEach { download ->
                    anidbJobSemaphore.withPermit {
                        koin.get<AniDBJob>(parameters = { parametersOf(download) }).schedule {
                            try {
                                launch {
                                    newSuspendedTransaction {
                                        download.status = Matching
                                    }
                                    it()
                                }.asCompletableFuture()
                                    .let {
                                        if (flushAnidbJob.get().isActive)
                                            it.orTimeout(10, TimeUnit.SECONDS)
                                        else
                                            it
                                    }
                                    .join()
                            } catch (e: Exception) {
                                debug { "reached buffer limit for anidb jobs" }
                                flushAnidbJobs()
                                newSuspendedTransaction {
                                    download.status = Hashed
                                }
                            }
                        }
                    }
                }
        }
    }

    private fun scheduleAnidbLogout() = launchDaemon("anidb logout") {
        if (anidb.lastSuccessfulSessionRequest().plus(1.minutes) < Clock.System.now() && anidb.isLoggedIn()) {
            debug { "anidb logout" }
            anidb.logout()
        }
    }

    private fun scheduleAnidbFlush()=launchDaemon("anidb flush",1.minutes){
        checkFlush()
    }

    private fun scheduleRename() = launchDaemon("rename") {
        newSuspendedTransaction {
            Download.find {
                (DownloadTable.status eq Matched) and
                        (DownloadTable.ed2k neq null)
            }
                .filter {
                    fileDownloader.fileExists(it)
                }
                .forEach {
                    koin.get<RenameFile>(parameters = { parametersOf(it) }).schedule()
                }
        }
    }

    private suspend fun AnimeJob.schedule(actionFrame: suspend (suspend () -> Unit) -> Unit = { it() }) {
        debug { "scheduling ${this::class}" }
        actionFrame { channelForJob(this::class).send(this) }
    }

    private fun launchDaemon(name: String, delay: Duration = 1.seconds, block: suspend CoroutineScope.() -> Unit) = CoroutineScope(Dispatchers.IO)
        .launch(CoroutineName("$name daemon")) {
            info { "starting daemon $name" }
            while (true) {
                block()
                delay(delay)
            }
        }
}
