package de.comhix.anime.downloader.job

import de.comhix.anidb.util.ed2kSum
import de.comhix.anime.downloader.data.Download
import de.comhix.anime.downloader.data.DownloadStatus.*
import de.comhix.anime.sharedCode.logger.info
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import java.io.File

/**
 * @author Benjamin Beeker
 */
class Ed2kHash(private val downloadFolder: File, private val download: Download) : AnimeJob {
    override suspend fun execute() {
        newSuspendedTransaction {
            download.status = Hashing
        }
        try {
            val file = downloadFolder.resolve(download.name)

            info { "start hashing $file" }

            val ed2k = file.inputStream().use {
                it.ed2kSum().joinToString(separator = "") { byte ->
                    String.format("%02x", byte)
                }
            }

            newSuspendedTransaction {
                download.ed2k = ed2k
                download.status = Hashed
            }
        } catch (e: Exception) {
            newSuspendedTransaction {
                download.status = Downloaded
            }
            throw e
        }
    }
}