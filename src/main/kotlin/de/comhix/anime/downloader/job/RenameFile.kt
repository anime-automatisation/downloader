package de.comhix.anime.downloader.job

import de.comhix.anidb.Anidb
import de.comhix.anidb.request.MylistState
import de.comhix.anime.downloader.config.Config
import de.comhix.anime.downloader.config.RenameConfig
import de.comhix.anime.downloader.config.jobModule
import de.comhix.anime.downloader.config.productionModule
import de.comhix.anime.downloader.data.Download
import de.comhix.anime.downloader.data.DownloadStatus.*
import de.comhix.anime.downloader.data.anidb.Anime
import de.comhix.anime.downloader.data.anidb.Episode
import de.comhix.anime.downloader.data.anidb.File
import de.comhix.anime.downloader.data.anidb.FileTable
import de.comhix.anime.sharedCode.logger.debug
import de.comhix.anime.sharedCode.logger.info
import de.comhix.anime.sharedCode.logger.warn
import kotlinx.coroutines.delay
import kotlinx.datetime.Instant
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.context.startKoin
import org.luaj.vm2.Globals
import org.luaj.vm2.LoadState
import org.luaj.vm2.LuaValue
import org.luaj.vm2.LuaValue.Companion.tableOf
import org.luaj.vm2.compiler.LuaC
import org.luaj.vm2.lib.*
import java.io.IOException
import java.nio.file.Path
import java.nio.file.StandardCopyOption
import java.util.*
import kotlin.io.path.absolute
import kotlin.io.path.createDirectories
import kotlin.io.path.moveTo
import kotlin.reflect.full.declaredMemberProperties

/**
 * @author Benjamin Beeker
 */
class RenameFile(
    private val config: Config,
    private val download: Download,
    private val anidb: Anidb
) : AnimeJob {
    override suspend fun execute() {
        try {
            newSuspendedTransaction {
                download.status = Renaming
            }
            newSuspendedTransaction {
                if (!anidb.isLoggedIn()) {
                    anidb.login()
                }

                val file = File.find {
                    FileTable.ed2k eq download.ed2k!!
                }.firstOrNull() ?: run {
                    debug { "file for ed2k hash ${download.ed2k} not found" }
                    return@newSuspendedTransaction
                }

                val episode = Episode.findById(file.episodeId) ?: run {
                    debug { "episode ${file.episodeId} not found" }
                    return@newSuspendedTransaction
                }

                val anime = Anime.findById(episode.animeId) ?: run {
                    debug { "anime ${episode.animeId} not found" }
                    return@newSuspendedTransaction
                }

                val newFilename = getNewFilename(config.renameConfig.lua, RenameData(file, episode, anime))

                val sourceFile = Path.of(config.downloadFolder, download.name).absolute()
                val targetFile = Path.of(config.renameConfig.baseDir, newFilename).absolute()

                info { "trying to copy $sourceFile to $targetFile" }

                targetFile.parent.createDirectories()

                tryMove(sourceFile, targetFile)

                anidb.addToMyList(file.id.value, Date(), MylistState.INTERNAL_STORAGE)

                download.status = Renamed
            }
        } catch (t: Throwable) {
            newSuspendedTransaction {
                download.status = Matched
            }
            throw t
        }
    }

    private suspend fun tryMove(file: Path, wantedFile: Path, allowedFails: Int = 3) {
        try {
            file.moveTo(wantedFile, StandardCopyOption.REPLACE_EXISTING)
        } catch (exception: IOException) {
            if (allowedFails <= 0) {
                warn { "could not move file: ${exception.stackTraceToString()}" }
                throw exception
            } else {
                delay(1000)
                tryMove(file, wantedFile, allowedFails - 1)
            }
        }
    }
}

private fun getNewFilename(lua: String, data: RenameData): String {
    val globals = Globals().apply {
//        load(BaseLib())
        load(PackageLib())
//        load(Bit32Lib())
//        load(TableLib())
        load(StringLib())
//        load(CoroutineLib())
        LoadState.install(this)

        LuaC.install(this)
    }

    val code = globals.load(lua)
    val arg = data.toLua()
    return code.call(arg).strvalue()!!.toString()
}

data class RenameData(val file: File, val episode: Episode, val anime: Anime)

private fun Any?.toLua(): LuaValue {

    return when (this) {
        null -> LuaValue.NIL
        is Int -> LuaValue.valueOf(this)
        is String -> LuaValue.valueOf(this)
        is Double -> LuaValue.valueOf(this)
        is Boolean -> LuaValue.valueOf(this)
        is Instant -> LuaValue.valueOf(this.toString())
        is Enum<*> -> LuaValue.valueOf(this.name)
        is ByteArray -> LuaValue.valueOf(this)
        else -> {
            val values = this::class.declaredMemberProperties.flatMap {
                val value = it.call(this)
                listOf(
                    LuaValue.valueOf(it.name),
                    value.toLua()
                )
            }

            val valuesWithId = values + if (this is IntEntity) {
                listOf(LuaValue.valueOf("id"), LuaValue.valueOf(this.id.value))
            } else {
                emptyList()
            }

            tableOf(valuesWithId.toTypedArray())
        }
    }
}


fun main() {
    val koin = startKoin { modules(productionModule, jobModule) }.koin.also { it.createEagerInstances() }
    val config = koin.get<RenameConfig>()
    transaction {
        val data = RenameData(
            File.findById(3483867)!!,
            Episode.findById(284117)!!,
            Anime.findById(18317)!!
        )

        // language=lua
        val script = """
        args = ...
    
        notAllowed="[.:!/<>|\"\\?]"

        year = args.anime.year:sub(0,4)
        
        if args.episode.episodeType == "SPECIAL"
        then
            season = "00"
        else
            season = "01"
        end

        if args.anime.englishName ~= "" 
        then
            anime = args.anime.englishName
        elseif args.anime.romajiName ~= "" then
            anime = args.anime.romajiName
        else
            anime = args.anime.kanjiName
        end

        anime = anime:gsub(notAllowed,"")

        if args.episode.englishName ~= "" then
            episode = args.episode.englishName
        elseif args.episode.romajiName ~= "" then
            episode = args.episode.romajiName
        else
            episode = args.episode.kanjiName
        end

        episode = episode:gsub(notAllowed,"")
        episodeNr = args.episode.typedEpisodeNumber

        return string.format("%s/%s[%d]/Season %s/%s - S%sE%s - %s [%s][%s].%s",
                year,
                anime,
                args.anime.id,
                season, 
                anime,
                season,
                episodeNr,
                episode,
                args.file.fileSource,
                args.file.crc,
                args.file.fileType
               )
    """.trimIndent()
        val newName = getNewFilename(
            config.lua,
//            script,
            data
        )
        println(newName)
    }
}