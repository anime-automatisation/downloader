package de.comhix.anime.downloader.job

import de.comhix.anime.downloader.data.Download
import de.comhix.anime.downloader.service.FileDownloader

/**
 * @author Benjamin Beeker
 */
class DownloadFile(private val fileDownloader: FileDownloader,private val download: Download) :AnimeJob {
    override suspend fun execute() {
        fileDownloader.downloadFile(download)
    }
}