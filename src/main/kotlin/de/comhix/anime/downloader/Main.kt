package de.comhix.anime.downloader

import de.comhix.anime.downloader.config.jobModule
import de.comhix.anime.downloader.config.productionModule
import de.comhix.anime.downloader.data.Download
import de.comhix.anime.downloader.data.DownloadStatus.*
import de.comhix.anime.downloader.data.DownloadTable
import de.comhix.anime.downloader.job.Scheduler
import kotlinx.coroutines.delay
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.koin.core.context.startKoin
import kotlin.time.Duration.Companion.seconds

/**
 * @author Benjamin Beeker
 */
suspend fun main() {
    val koin = startKoin { modules(productionModule, jobModule) }.koin.also { it.createEagerInstances() }

    val scheduler: Scheduler = koin.get()

    newSuspendedTransaction {
        Download.find { DownloadTable.status eq Downloading }
            .forEach { it.status = Error }
        Download.find { DownloadTable.status eq Hashing }
            .forEach { it.status = Downloaded }
        Download.find { DownloadTable.status eq Matching }
            .forEach { it.status = Hashed }
        Download.find { DownloadTable.status eq Renaming }
            .forEach { it.status = Matched }
    }

    scheduler.startSchedules()
    delay(5.seconds)
}