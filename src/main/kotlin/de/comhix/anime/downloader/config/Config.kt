package de.comhix.anime.downloader.config

import kotlinx.serialization.Serializable

/**
 * @author Benjamin Beeker
 */
@Serializable
data class Config(
    val apiEndpoint: String,
    val downloadEndpoint: String,
    val apiToken: String,
    val downloadFolder: String,
    val database: DatabaseConfig,
    val scheduler: SchedulerConfig,
    val anidbConfig: AnidbConfig,
    val renameConfig: RenameConfig
)

@Serializable
data class DatabaseConfig(
    val driver:String,
    val url: String,
    val username: String,
    val password: String
)

@Serializable
data class SchedulerConfig(
    val suicideMonitor: Boolean = true,
    val anidbBuffer: Int = 10,
    val maxParallelHashing: Int = 1,
    val maxParallelDownloads: Int = 1,
    val maxParallelRenaming: Int = 1
)

@Serializable
data class AnidbConfig(
    val username: String,
    val password: String,
    val clientId: String,
    val clientVersion: Int
)

@Serializable
data class RenameConfig(
    val lua: String,
    val baseDir: String
)