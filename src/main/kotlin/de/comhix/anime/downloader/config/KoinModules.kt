package de.comhix.anime.downloader.config

import de.comhix.anidb.AnidbProvider
import de.comhix.anime.downloader.api.Client
import de.comhix.anime.downloader.data.DownloadTable
import de.comhix.anime.downloader.data.anidb.AnimeTable
import de.comhix.anime.downloader.data.anidb.EpisodeTable
import de.comhix.anime.downloader.data.anidb.FileTable
import de.comhix.anime.downloader.job.*
import de.comhix.anime.downloader.service.FileDownloader
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import java.io.File

/**
 * @author Benjamin Beeker
 */
@OptIn(ExperimentalSerializationApi::class)
val productionModule = module {
    single<Config> {
        Json.decodeFromStream(File(System.getenv("CONFIG_FILE") ?: ".local/config-dev.json").inputStream())
    }
    single {
        get<Config>().renameConfig
    }
    single(createdAtStart = true) {
        val config: Config = get()
        TransactionManager.defaultDatabase = Database.connect(
            url = config.database.url,
            driver = config.database.driver,
            user = config.database.username,
            password = config.database.password
        ).also {

            transaction {
                SchemaUtils.createMissingTablesAndColumns(
                    DownloadTable,
                    FileTable,
                    EpisodeTable,
                    AnimeTable,
                )
            }
        }
    }
    single {
        File(get<Config>().downloadFolder).also { it.mkdirs() }
    }
    singleOf(::Client)
    singleOf(::FileDownloader)
    singleOf(::Scheduler)
    single {
        getKoin()
    }
    single {
        val config: Config = get()
        val provider = AnidbProvider(
            config.anidbConfig.clientId,
            config.anidbConfig.clientVersion
        )
        provider.get(
            config.anidbConfig.username,
            config.anidbConfig.password
        )
    }
}

val jobModule = module {
    factoryOf(::DownloadFile)
    factoryOf(::Ed2kHash)
    factoryOf(::AniDBJob)
    factoryOf(::RenameFile)
}