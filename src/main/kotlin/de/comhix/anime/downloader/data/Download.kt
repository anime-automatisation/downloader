package de.comhix.anime.downloader.data

import kotlinx.datetime.Instant
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.kotlin.datetime.timestamp

/**
 * @author Benjamin Beeker
 */
internal object DownloadTable : IntIdTable("download") {
    val name = text("name").uniqueIndex()
    val lastModified = timestamp("last_modified")
    val size = long("size")
    val sha256 = varchar("sha256", 64)
    val ed2k = varchar("ed2k", 32).nullable()
    val status = enumerationByName<DownloadStatus>("status", DownloadStatus.entries.maxOf { it.name.length })
    val lastLookup = timestamp("last_lookup").default(Instant.fromEpochMilliseconds(0))
}

class Download(id: EntityID<Int>) : IntEntity(id) {
    var name by DownloadTable.name
    var lastModified by DownloadTable.lastModified
    var size by DownloadTable.size
    var sha256 by DownloadTable.sha256
    var ed2k by DownloadTable.ed2k
    var status by DownloadTable.status
    var lastLookup by DownloadTable.lastLookup

    companion object : IntEntityClass<Download>(DownloadTable)
}

enum class DownloadStatus {
    New,
    Downloading,
    Error,
    Removed,
    Downloaded,
    Hashing,
    Hashed,
    Matching,
    Matched,
    Renaming,
    Renamed
}