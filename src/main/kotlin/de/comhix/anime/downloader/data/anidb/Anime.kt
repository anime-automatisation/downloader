package de.comhix.anime.downloader.data.anidb

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.kotlin.datetime.timestamp

/**
 * @author Benjamin Beeker
 */

internal object AnimeTable : IntIdTable("anidb_anime") {
    //    val relatedAnimeIds: List<Int>? = null,
//    val relatedAnimeTypes: List<Int>? = null,
    //    val shortNames: List<String>? = null,
//    val synonyms: List<String>? = null,
    //    val awards: List<String>? = null,
    //    val tagNames: List<String>? = null,
//    val tagIds: List<Int>? = null,
//    val tagWeights: List<Int>? = null,
    //    val characters: List<Int>? = null,

    val dateFlags = integer("date_flags")
    val year = varchar("year", 255)
    val type = varchar("type", 255)
    val romajiName = varchar("romaji_name", 255)
    val kanjiName = varchar("kanji_name", 255)
    val englishName = varchar("english_name", 255)
    val otherName = text("other_name")
    val episodes = integer("episodes")
    val highestEpisodeNumber = integer("highest_episode_number")
    val specialEpisodes = integer("special_episodes")
    val airDate = timestamp("air_date")
    val endDate = timestamp("end_date")
    val url = varchar("url", 255)
    val picname = varchar("picname", 255)
    val rating = integer("rating")
    val votes = integer("votes")
    val temporaryRating = integer("temporary_rating")
    val temporaryVotes = integer("temporary_votes")
    val averageReviewRating = integer("average_review_rating")
    val reviews = integer("reviews")
    val adult = bool("adult")
    val annId = integer("ann_id")
    val allcinemaId = integer("allcinema_id")
    val animeNfoId = varchar("anime_nfo_id", 255)
    val specials = integer("specials")
    val credits = integer("credits")
    val others = integer("others")
    val trailers = integer("trailers")
    val parodys = integer("parodys")
    val lastUpdate = timestamp("last_update")
}

class Anime(id: EntityID<Int>) : IntEntity(id), Timestamped {
    var dateFlags by AnimeTable.dateFlags
    var year by AnimeTable.year
    var type by AnimeTable.type
    var romajiName by AnimeTable.romajiName
    var kanjiName by AnimeTable.kanjiName
    var englishName by AnimeTable.englishName
    var otherName by AnimeTable.otherName
    var episodes by AnimeTable.episodes
    var highestEpisodeNumber by AnimeTable.highestEpisodeNumber
    var specialEpisodes by AnimeTable.specialEpisodes
    var airDate by AnimeTable.airDate
    var endDate by AnimeTable.endDate
    var url by AnimeTable.url
    var picname by AnimeTable.picname
    var rating by AnimeTable.rating
    var votes by AnimeTable.votes
    var temporaryRating by AnimeTable.temporaryRating
    var temporaryVotes by AnimeTable.temporaryVotes
    var averageReviewRating by AnimeTable.averageReviewRating
    var reviews by AnimeTable.reviews
    var adult by AnimeTable.adult
    var annId by AnimeTable.annId
    var allcinemaId by AnimeTable.allcinemaId
    var animeNfoId by AnimeTable.animeNfoId
    var specials by AnimeTable.specials
    var credits by AnimeTable.credits
    var others by AnimeTable.others
    var trailers by AnimeTable.trailers
    var parodys by AnimeTable.parodys
    override var lastUpdate by AnimeTable.lastUpdate

    companion object : IntEntityClass<Anime>(AnimeTable)

    override fun toString(): String {
        return "Anime(dateFlags=$dateFlags, year='$year', type='$type', romajiName='$romajiName', kanjiName='$kanjiName', englishName='$englishName', otherName='$otherName', episodes=$episodes, highestEpisodeNumber=$highestEpisodeNumber, specialEpisodes=$specialEpisodes, airDate=$airDate, endDate=$endDate, url='$url', picname='$picname', rating=$rating, votes=$votes, temporaryRating=$temporaryRating, temporaryVotes=$temporaryVotes, averageReviewRating=$averageReviewRating, reviews=$reviews, adult=$adult, annId=$annId, allcinemaId=$allcinemaId, animeNfoId='$animeNfoId', specials=$specials, credits=$credits, others=$others, trailers=$trailers, parodys=$parodys, lastUpdate=$lastUpdate)"
    }
}