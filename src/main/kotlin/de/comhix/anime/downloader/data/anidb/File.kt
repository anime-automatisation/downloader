package de.comhix.anime.downloader.data.anidb

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.kotlin.datetime.timestamp

/**
 * @author Benjamin Beeker
 */
internal object FileTable : IntIdTable("anidb_file") {
    val episodeId = integer("episode_id")
    val ed2k = varchar("ed2k", 32).uniqueIndex()
    val crc = varchar("crc", 8)
    val fileSource = varchar("source", 255)
    val fileType = varchar("file_type", 255)
    val state = integer("state")
    val lastUpdate = timestamp("last_update")
}

class File(id: EntityID<Int>) : IntEntity(id), Timestamped {
    var episodeId by FileTable.episodeId
    var ed2k by FileTable.ed2k
    var crc by FileTable.crc
    var fileSource by FileTable.fileSource
    var fileType by FileTable.fileType
    var state by FileTable.state
    override var lastUpdate by FileTable.lastUpdate



    companion object : IntEntityClass<File>(FileTable)

    override fun toString(): String {
        return "File(episodeId=$episodeId, ed2k='$ed2k', crc='$crc', fileSource='$fileSource', fileType='$fileType', state=$state, lastUpdate=$lastUpdate)"
    }
}