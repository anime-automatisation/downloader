package de.comhix.anime.downloader.data.anidb

import kotlinx.datetime.Instant

interface Timestamped {
    var lastUpdate: Instant
}