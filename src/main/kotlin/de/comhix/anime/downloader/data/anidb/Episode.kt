package de.comhix.anime.downloader.data.anidb

import de.comhix.anidb.data.EpisodeType
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.kotlin.datetime.timestamp

/**
 * @author Benjamin Beeker
 */

internal object EpisodeTable : IntIdTable("anidb_episode") {
    val animeId = integer("anime_id")
    val length = integer("length")
    val rating = integer("rating")
    val votes = integer("votes")
    val typedEpisodeNumber = varchar("typed_episode_number", 255)
    val romajiName = varchar("romaji_name", 255)
    val kanjiName = varchar("kanji_name", 255)
    val englishName = varchar("english_name", 255)
    val airedDate = timestamp("aired_date")
    val episodeType = enumerationByName<EpisodeType>("episode_type", EpisodeType.entries.maxOf { it.name.length })
    val lastUpdate = timestamp("last_update")
}

class Episode(id: EntityID<Int>) : IntEntity(id), Timestamped {
    var animeId by EpisodeTable.animeId
    var length by EpisodeTable.length
    var rating by EpisodeTable.rating
    var votes by EpisodeTable.votes
    var typedEpisodeNumber by EpisodeTable.typedEpisodeNumber
    var romajiName by EpisodeTable.romajiName
    var kanjiName by EpisodeTable.kanjiName
    var englishName by EpisodeTable.englishName
    var airedDate by EpisodeTable.airedDate
    var episodeType by EpisodeTable.episodeType
    override var lastUpdate by EpisodeTable.lastUpdate

    companion object : IntEntityClass<Episode>(EpisodeTable)

    override fun toString(): String {
        return "Episode(animeId=$animeId, length=$length, rating=$rating, votes=$votes, typedEpisodeNumber='$typedEpisodeNumber', romajiName='$romajiName', kanjiName='$kanjiName', englishName='$englishName', airedDate=$airedDate, episodeType=$episodeType, lastUpdate=$lastUpdate)"
    }
}