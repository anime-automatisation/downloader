package de.comhix.anime.downloader.pipeline

import org.amshove.kluent.`should be equal to`
import org.luaj.vm2.Globals
import org.luaj.vm2.LoadState
import org.luaj.vm2.LuaValue
import org.luaj.vm2.LuaValue.Companion.tableOf
import org.luaj.vm2.compiler.LuaC
import org.luaj.vm2.lib.*
import kotlin.test.Test

/**
 * @author Benjamin Beeker
 */
class LuaFooTest {
    data class Data(val name: String, val episode: Int)

    @Test
    fun `test foobar`() {
        val globals = Globals().apply {
//            load(BaseLib())
//            load(Bit32Lib())
//            load(TableLib())
//            load(StringLib())
//            load(CoroutineLib())
            LoadState.install(this)

            load(PackageLib())
            LuaC.install(this)
        }

        val code = globals.load(
            // language=lua
            """
                argument = ...
                return argument.name .. ": " .. argument.episode
        """.trimIndent()
        )

        val data = Data("my name", 5)

        tableOf(
            arrayOf(
                LuaValue.valueOf(data::name.name),
                LuaValue.valueOf(data.name),
                LuaValue.valueOf(data::episode.name),
                LuaValue.valueOf(data.episode)
            )
        )
        val string = code.call(
            tableOf(
                arrayOf(
                    LuaValue.valueOf("name"),
                    LuaValue.valueOf(data.name),
                    LuaValue.valueOf("episode"),
                    LuaValue.valueOf(data.episode)
                )
            )
        ).strvalue()?.toString()

        string `should be equal to` "${data.name}: ${data.episode}"
    }
}